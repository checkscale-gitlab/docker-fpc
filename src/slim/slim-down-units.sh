#!/usr/bin/env sh

set -euxo pipefail

LIB_FPC=$1

# Sanity checks
[ -d $LIB_FPC ] || exit 1
[ $(basename $LIB_FPC) == "fpc" ] || exit 1

# Be version and architecture agnostic
UNITS=$(find $LIB_FPC/*/units/*/ -type d -maxdepth 0)
[ $(echo $UNITS | wc -w) -gt 0 ] || exit 1

# Slim down to basic units
find $UNITS -type d -mindepth 1 -maxdepth 1 \
	-not -name 'fcl-base' \
	-not -name 'rtl' \
	-not -name 'rtl-console' \
	-not -name 'rtl-objpas' \
	-exec rm -r {} \;
